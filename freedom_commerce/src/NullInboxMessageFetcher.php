<?php

namespace Drupal\freedom_commerce;

use \Drupal\commerce\InboxMessageFetcherInterface;

/**
 * Provides the InboxMessageFetcher service.
 */
class NullInboxMessageFetcher implements InboxMessageFetcherInterface {

  /**
   * {@inheritdoc}
   */
  public function fetch(): void {}

  /**
   * {@inheritdoc}
   */
  public function fetchNewStoreMessages(): void {}

}
